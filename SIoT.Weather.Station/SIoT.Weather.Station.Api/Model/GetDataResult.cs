﻿namespace SIoT.Weather.Station.Api.Model
{
    public class GetDataResult<T>
    {
        public bool Success { get; set; }
        public T Data { get; set; }
    }
}
