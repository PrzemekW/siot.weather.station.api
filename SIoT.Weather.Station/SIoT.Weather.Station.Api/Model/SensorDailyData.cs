﻿using System.Collections.Generic;

namespace SIoT.Weather.Station.Api.Model
{
    public class SensorDailyData
    {
        public List<SensorDataPoint> Temperature { get; set; }
        public List<SensorDataPoint> Humidity { get; set; }
        public List<SensorDataPoint> RainFall { get; set; }
    }
}
