﻿namespace SIoT.Weather.Station.Api.Model
{
    public class BlobByteResult
    {
        public bool HistoricalData { get; set; }
        public byte[] Bytes { get; set; }
    }
}
