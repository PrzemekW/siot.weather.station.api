﻿using System;

namespace SIoT.Weather.Station.Api.Model
{
    public class SensorDataPoint
    {
        public DateTime Timestamp { get; set; }
        public double Value { get; set; }
    }
}
