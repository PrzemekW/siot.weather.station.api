﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SIoT.Weather.Station.Api.Interface;
using SIoT.Weather.Station.Api.Model;

namespace SIoT.Weather.Station.Api.ApplicationService
{
    public class SensorDataApplicationService : ISensorDataApplicationService
    {
        private readonly ISensorDataDomainService _sensorDataDomainService;

        public SensorDataApplicationService(ISensorDataDomainService sensorDataDomainService)
        {
            _sensorDataDomainService = sensorDataDomainService;
        }

        public async Task<GetDataResult<List<SensorDataPoint>>> GetAllDataPointsOfOneDayForOneSensorType(string deviceId, DateTime date, string sensorType)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                throw new ArgumentNullException(nameof(deviceId));
            }

            if (string.IsNullOrWhiteSpace(sensorType))
            {
                throw new ArgumentNullException(nameof(sensorType));
            }

            if (date == default)
            {
                throw new ArgumentException($"{nameof(date)} cannot be default date value({default(DateTime)}");
            }

            var dataPointList =
                await _sensorDataDomainService.GetAllDataPointsOfOneDayForOneSensorType(deviceId, date, sensorType);

            if (dataPointList == null)
            {
                return new GetDataResult<List<SensorDataPoint>>
                {
                    Success = false
                };
            }

            return new GetDataResult<List<SensorDataPoint>>
            {
                Success = true,
                Data = dataPointList
            };
        }

        public async Task<GetDataResult<SensorDailyData>> GetDataPointsOfOneDay(string deviceId, DateTime date)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                throw new ArgumentNullException(nameof(deviceId));
            }

            if (date == default)
            {
                throw new ArgumentException($"{nameof(date)} cannot be default date value({default(DateTime)}");
            }

            var temperatureDataPointList =
                await _sensorDataDomainService.GetAllDataPointsOfOneDayForOneSensorType(deviceId, date, "temperature");

            var humidityDataPointList =
                await _sensorDataDomainService.GetAllDataPointsOfOneDayForOneSensorType(deviceId, date, "humidity");

            var rainfallDataPointList =
                await _sensorDataDomainService.GetAllDataPointsOfOneDayForOneSensorType(deviceId, date, "rainfall");

            if (temperatureDataPointList == null &&
                humidityDataPointList == null &&
                rainfallDataPointList == null)
            {
                return new GetDataResult<SensorDailyData>
                {
                    Success = false
                };
            }

            return new GetDataResult<SensorDailyData>
            {
                Success = true,
                Data = new SensorDailyData
                {
                    Humidity = humidityDataPointList,
                    RainFall = rainfallDataPointList,
                    Temperature = temperatureDataPointList
                }
            };
        }
    }
}