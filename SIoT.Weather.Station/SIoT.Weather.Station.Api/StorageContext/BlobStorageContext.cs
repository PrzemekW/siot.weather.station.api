﻿using System;
using Azure.Storage.Blobs;
using SIoT.Weather.Station.Api.Interface;

namespace SIoT.Weather.Station.Api.StorageContext
{
    public class BlobStorageContext : IBlobStorageContext
    {
        private readonly string _containerName;
        private readonly BlobServiceClient _blobServiceClient;
        
        public BlobStorageContext(string connectionString, string containerName)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            if (string.IsNullOrWhiteSpace(containerName))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            _containerName = containerName;
            _blobServiceClient = new BlobServiceClient(connectionString);
        }

        public BlobContainerClient BlobContainerClient => _blobServiceClient.GetBlobContainerClient(_containerName);
    }
}