﻿using System;
using System.Collections.Concurrent;
using SIoT.Weather.Station.Api.Interface;

namespace SIoT.Weather.Station.Api.Cache
{
    public class HistoricalSensorDataCache : IHistoricalSensorDataCache
    {
        private readonly object _lockObject = new object();
        private DateTime _updateTimestamp;

        public DateTime UpdateTimestamp
        {
            get
            {
                lock (_lockObject)
                {
                    return _updateTimestamp;
                }
            }
            set
            {
                lock (_lockObject)
                {
                    _updateTimestamp = value;
                }
            }
        }

        public ConcurrentDictionary<string, byte[]> CachedData { get; set; } = new ConcurrentDictionary<string, byte[]>();
    }
}