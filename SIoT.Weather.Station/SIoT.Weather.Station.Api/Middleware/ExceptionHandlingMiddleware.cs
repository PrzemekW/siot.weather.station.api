﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using SIoT.Weather.Station.Api.Extensions;

namespace SIoT.Weather.Station.Api.Middleware
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandlingMiddleware> _logger;

        public ExceptionHandlingMiddleware(RequestDelegate next, ILogger<ExceptionHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                var failedRequestInfo = $"{context.Request.Scheme.ToUpperInvariant()} {context.Request.Method} {context.Request.Host}{context.Request.Path}{context.Request.QueryString}";
                _logger.LogError($"{ex.Message} {failedRequestInfo}");
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Response.SetErrorMessage(ex.Message);
            }
        }
    }
}
