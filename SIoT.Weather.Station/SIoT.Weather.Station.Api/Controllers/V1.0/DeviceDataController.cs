﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIoT.Weather.Station.Api.Interface;

namespace SIoT.Weather.Station.Api.Controllers.V1._0
{
    [Produces("application/json")]
    [ApiController]
    public class DeviceDataController : ControllerBase
    {
        private readonly IRequestParameterValidator _requestParameterValidator;
        private readonly ISensorDataApplicationService _sensorDataApplicationService;

        public DeviceDataController(IRequestParameterValidator requestParameterValidator,
            ISensorDataApplicationService sensorDataApplicationService)
        {
            _requestParameterValidator = requestParameterValidator;
            _sensorDataApplicationService = sensorDataApplicationService;
        }

        /// <summary>
        /// Get all data points for one unit and one day
        /// </summary>
        /// <remarks>
        /// Example request:
        ///     GET api/v1/devices/testdevice/data/2020-01-01/temperature
        /// </remarks>
        /// <param name="deviceId">Device Id e.g testdevice</param>
        /// <param name="date">Date e.g. 2020-01-01</param>
        /// <param name="sensorType">Type of sensor e.g. temperature</param>
        /// <returns>A collection of data points</returns>
        /// <response code="200">Returns a collection of data points</response>
        /// <response code="404">When there is no data found for requested condition</response>
        /// <response code="400">When required parameter is missing or invalid</response>
        [HttpGet]
        [Route("api/v{version:apiVersion}/devices/{deviceId}/data/{date}/{sensorType}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllDataPointsOfOneDayForOneSensorType(string deviceId, DateTime date, string sensorType)
        {
            if (!_requestParameterValidator.ParametersForGetAllDataPointsOfOneDayForOneSensorTypeAreValid(deviceId,
                date, sensorType))
            {
                return BadRequest();
            }

            var getDataResult =
               await _sensorDataApplicationService.GetAllDataPointsOfOneDayForOneSensorType(deviceId, date, sensorType);

            if (!getDataResult.Success)
            {
                return NotFound();
            }
            return Ok(getDataResult.Data);
        }


        /// <summary>
        /// Get all data points for one unit and one day
        /// </summary>
        /// <remarks>
        /// Example request:
        ///     GET getdata?deviceId=testdevice&amp;date=2020-01-01&amp;sensorType=temperature
        /// </remarks>
        /// <param name="deviceId">Device Id e.g testdevice</param>
        /// <param name="date">Date e.g. 2020-01-01</param>
        /// <param name="sensorType">Type of sensor e.g. temperature</param>
        /// <returns>A collection of data points</returns>
        /// <response code="200">Returns a collection of data points</response>
        /// <response code="404">When there is no data found for requested condition</response>
        /// <response code="400">When required parameter is missing or invalid</response>
        [HttpGet]
        [Route("getdata")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllDataPointsOfOneDayForOneSensorTypeWithQueryString([FromQuery] string deviceId,
            [FromQuery] DateTime date, [FromQuery] string sensorType)
        {
            return await GetAllDataPointsOfOneDayForOneSensorType(deviceId, date, sensorType);
        }


        /// <summary>
        /// Get temperature, humidity and rainfall for a day.
        /// </summary>
        /// <remarks>
        /// Example request:
        ///     GET api/v1/devices/testdevice/data/2020-01-01
        /// </remarks>
        /// <param name="deviceId">Device Id e.g. testdevice</param>
        /// <param name="date">Date e.g. 2020-01-01</param>
        /// <returns>A sensor daily data</returns>
        /// <response code="200">Returns an object with sensor daily data</response>
        /// <response code="404">When there is no data found for requested condition</response>
        /// <response code="400">When required parameter is missing or invalid</response>
        [HttpGet]
        [Route("api/v{version:apiVersion}/devices/{deviceId}/data/{date}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDataPointsOfOneDay(string deviceId, DateTime date)
        {
            if (!_requestParameterValidator.ParametersForGetDataPointsOfOneDayAreValid(deviceId, date))
            {
                return BadRequest();
            }

            var sensorDailyData = await _sensorDataApplicationService.GetDataPointsOfOneDay(deviceId,
                date);

            if (!sensorDailyData.Success)
            {
                return NotFound();
            }

            return Ok(sensorDailyData.Data);
        }

        /// <summary>
        /// Get temperature, humidity and rainfall for a day.
        /// </summary>
        /// <remarks>
        /// Example request:
        ///     GET getdatafordevice?deviceId=testdevice&amp;date=2020-01-01
        /// </remarks>
        /// <param name="deviceId">Device Id e.g. testdevice</param>
        /// <param name="date">Date e.g. 2020-01-01</param>
        /// <returns>A sensor daily data</returns>
        /// <response code="200">Returns an object with sensor daily data</response>
        /// <response code="404">When there is no data found for requested condition</response>
        /// <response code="400">When required parameter is missing or invalid</response>
        [HttpGet]
        [Route("getdatafordevice")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDataPointsOfOneDayWithQueryString([FromQuery]string deviceId, [FromQuery]DateTime date)
        {
            return await GetDataPointsOfOneDay(deviceId, date);
        }
    }
}
