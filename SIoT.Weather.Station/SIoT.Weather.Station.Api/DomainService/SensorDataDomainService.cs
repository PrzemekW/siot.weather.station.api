﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SIoT.Weather.Station.Api.Interface;
using SIoT.Weather.Station.Api.Model;

namespace SIoT.Weather.Station.Api.DomainService
{
    public class SensorDataDomainService : ISensorDataDomainService
    {
        private readonly ISensorDataRepository _sensorDataRepository;
        private readonly IBlobHelper _blobHelper;

        public SensorDataDomainService(ISensorDataRepository sensorDataRepository, IBlobHelper blobHelper)
        {
            _sensorDataRepository = sensorDataRepository;
            _blobHelper = blobHelper;
        }

        public async Task<List<SensorDataPoint>> GetAllDataPointsOfOneDayForOneSensorType(string deviceId, DateTime date, string sensorType)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                throw new ArgumentNullException(nameof(deviceId));
            }

            if (string.IsNullOrWhiteSpace(sensorType))
            {
                throw new ArgumentNullException(nameof(sensorType));
            }

            if (date == default)
            {
                throw new ArgumentException($"{nameof(date)} cannot be default date value({default(DateTime)}");
            }

            var blobBytesResult = await _sensorDataRepository.GetBlobBytes(deviceId, date, sensorType);

            if (blobBytesResult.Bytes.Length == 0)
            {
                return null;
            }

            var sensorDataPointList = await _blobHelper.GetSensorDataPoints(blobBytesResult, date);

            return sensorDataPointList;
        }
    }
}