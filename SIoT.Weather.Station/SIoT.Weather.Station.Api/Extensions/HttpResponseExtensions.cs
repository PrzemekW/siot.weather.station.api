﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace SIoT.Weather.Station.Api.Extensions
{
    public static class HttpResponseExtensions
    {
        public static void SetErrorMessage(this HttpResponse response, string message)
        {
            var messageObject = new { errorMessage = message };
            var messageString = JsonConvert.SerializeObject(messageObject);
            response.ContentType = "application/json";
            response.WriteAsync(messageString);
        }
    }
}
