﻿using System;
using System.Globalization;
using SIoT.Weather.Station.Api.Model;

namespace SIoT.Weather.Station.Api.Extensions
{
    public static class SensorDataPointStringExtensions
    {
        public static SensorDataPoint MapToSensorDataPoint(this string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return null;
            }

            if (DateTime.TryParse(s.Split(';')[0], out var timestamp) &&
                    double.TryParse(s.Split(';')[1], NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign,
                        new CultureInfo("en-US"), out var result))
            {
                return new SensorDataPoint
                {
                    Timestamp = timestamp,
                    Value = result
                };
            }

            return null;
        }
    }
}
