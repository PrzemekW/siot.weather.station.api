﻿using System;
using System.IO;
using System.Threading.Tasks;
using Azure;
using Microsoft.Extensions.Logging;
using SIoT.Weather.Station.Api.Interface;
using SIoT.Weather.Station.Api.Model;

namespace SIoT.Weather.Station.Api.Repository
{
    public class SensorDataRepository : ISensorDataRepository
    {
        private readonly IBlobStorageContext _storageContext;
        private readonly IBlobHelper _blobHelper;
        private readonly ILogger<SensorDataRepository> _logger;
        private readonly IHistoricalSensorDataCache _historicalSensorDataCache;

        public SensorDataRepository(IBlobStorageContext storageContext, IBlobHelper blobHelper,
            ILogger<SensorDataRepository> logger, IHistoricalSensorDataCache historicalSensorDataCache)
        {
            _storageContext = storageContext;
            _blobHelper = blobHelper;
            _logger = logger;
            _historicalSensorDataCache = historicalSensorDataCache;
        }

        public async Task<BlobByteResult> GetBlobBytes(string deviceId, DateTime date, string sensorType)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                throw new ArgumentNullException(nameof(deviceId));
            }

            if (string.IsNullOrWhiteSpace(sensorType))
            {
                throw new ArgumentNullException(nameof(sensorType));
            }

            if (date == default)
            {
                throw new ArgumentException($"{nameof(date)} cannot be default date value({default(DateTime)}");
            }

            var blobName = _blobHelper.BuildBlobName(deviceId, date, sensorType);
            using (var stream = new MemoryStream())
            {
                try
                {
                    var blobClient = _storageContext.BlobContainerClient.GetBlobClient(blobName);
                    await blobClient.DownloadToAsync(stream);

                    return new BlobByteResult
                    {
                        HistoricalData = false,
                        Bytes = stream.ToArray()
                    };
                }
                catch (RequestFailedException)
                {
                    _logger.LogInformation(
                        "Blob \"{0}\" could not be downloaded from current blobs. It will be tried to gather from historical ones",
                        blobName);
                }
            }

            var historicalBlobName = _blobHelper.BuildHistoricalBLobName(deviceId, sensorType);

            // simple caching (first call of the day for device / sensor will take long)
            // can be memory consuming in case of big amount of devices / sensors
            // in above case probably caching unzipped zip in different storage would be better
            if(DateTime.UtcNow.Date <= _historicalSensorDataCache.UpdateTimestamp.Date && 
               _historicalSensorDataCache.CachedData.TryGetValue($"{deviceId}_{sensorType}", out var cachedData))
            {
                return new BlobByteResult
                {
                    HistoricalData = true,
                    Bytes = cachedData
                };
            }
            
            using (var stream = new MemoryStream())
            {
                try
                {
                    var blobClient = _storageContext.BlobContainerClient.GetBlobClient(historicalBlobName);
                    await blobClient.DownloadToAsync(stream);

                    var bytesArray = stream.ToArray();

                    _historicalSensorDataCache.UpdateTimestamp = DateTime.UtcNow;
                    _historicalSensorDataCache.CachedData.TryAdd($"{deviceId}_{sensorType}", bytesArray);

                    return new BlobByteResult
                    {
                        HistoricalData = true,
                        Bytes = bytesArray
                    };
                }
                catch (RequestFailedException e)
                {
                    _logger.LogInformation(
                        "Blob \"{0}\" could not be downloaded. Exception message: {1}",
                        historicalBlobName, e.Message);
                }
            }

            return new BlobByteResult
            {
                HistoricalData = false,
                Bytes = new byte[0]
            };
        }
    }
}