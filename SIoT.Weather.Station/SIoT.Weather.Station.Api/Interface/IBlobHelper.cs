﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SIoT.Weather.Station.Api.Model;

namespace SIoT.Weather.Station.Api.Interface
{
    public interface IBlobHelper
    {
        string BuildBlobName(string deviceId, DateTime date, string sensorType);
        string BuildHistoricalBLobName(string deviceId, string sensorType);
        Task<List<SensorDataPoint>> GetSensorDataPoints(BlobByteResult blobBytesResult, DateTime date);
    }
}
