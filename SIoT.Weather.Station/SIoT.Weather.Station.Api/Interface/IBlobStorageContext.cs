﻿using Azure.Storage.Blobs;

namespace SIoT.Weather.Station.Api.Interface
{
    public interface IBlobStorageContext
    {
        BlobContainerClient BlobContainerClient { get; }
    }
}
