﻿using System;

namespace SIoT.Weather.Station.Api.Interface
{
    public interface IRequestParameterValidator
    {
        bool ParametersForGetAllDataPointsOfOneDayForOneSensorTypeAreValid(string deviceId, DateTime date,
            string sensorType);
        bool ParametersForGetDataPointsOfOneDayAreValid(string deviceId, DateTime date);
    }
}