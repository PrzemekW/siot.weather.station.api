﻿using System;
using System.Threading.Tasks;
using SIoT.Weather.Station.Api.Model;

namespace SIoT.Weather.Station.Api.Interface
{
    public interface ISensorDataRepository
    {
        Task<BlobByteResult> GetBlobBytes(string deviceId, DateTime date, string sensorType);
    }
}