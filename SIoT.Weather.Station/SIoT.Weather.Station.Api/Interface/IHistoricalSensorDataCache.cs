﻿using System;
using System.Collections.Concurrent;

namespace SIoT.Weather.Station.Api.Interface
{
    public interface IHistoricalSensorDataCache
    {
        DateTime UpdateTimestamp { get; set; }
        ConcurrentDictionary<string, byte[]> CachedData { get; set; }
    }
}
