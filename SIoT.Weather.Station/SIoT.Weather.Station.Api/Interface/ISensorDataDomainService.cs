﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SIoT.Weather.Station.Api.Model;

namespace SIoT.Weather.Station.Api.Interface
{
    public interface ISensorDataDomainService
    {
        Task<List<SensorDataPoint>> GetAllDataPointsOfOneDayForOneSensorType(string deviceId, DateTime date,
            string sensorType);
    }
}
