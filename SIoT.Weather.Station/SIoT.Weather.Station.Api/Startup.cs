﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using SIoT.Weather.Station.Api.ApplicationService;
using SIoT.Weather.Station.Api.Cache;
using SIoT.Weather.Station.Api.DomainService;
using SIoT.Weather.Station.Api.Helper;
using SIoT.Weather.Station.Api.Interface;
using SIoT.Weather.Station.Api.Middleware;
using SIoT.Weather.Station.Api.Repository;
using SIoT.Weather.Station.Api.StorageContext;

namespace SIoT.Weather.Station.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddApiVersioning(options =>
            {
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = ApiVersion.Default;
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Weather Station Api", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddSingleton<IBlobStorageContext, BlobStorageContext>(_ =>
            {
                var connectionString = Configuration.GetValue<string>("AzureIotHubConnectionString");
                if (string.IsNullOrWhiteSpace(connectionString))
                {
                    throw new ArgumentNullException(nameof(connectionString));
                }

                var containerName = Configuration.GetValue<string>("AzureIoTHubContainerName");
                if (string.IsNullOrWhiteSpace(containerName))
                {
                    throw new ArgumentNullException(nameof(containerName));
                }

                return new BlobStorageContext(connectionString, containerName);
            });

            services.AddSingleton<IRequestParameterValidator, RequestParameterValidator>();
            services.AddSingleton<IBlobHelper, BlobHelper>();
            services.AddSingleton<IHistoricalSensorDataCache, HistoricalSensorDataCache>();
            services.AddScoped<ISensorDataApplicationService, SensorDataApplicationService>();
            services.AddScoped<ISensorDataDomainService, SensorDataDomainService>();
            services.AddScoped<ISensorDataRepository, SensorDataRepository>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseDeveloperExceptionPage();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Weather Station Api");
                    c.RoutePrefix = string.Empty;
                });
            }

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseMvc();
        }
    }
}
