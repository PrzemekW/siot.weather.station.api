﻿using System;
using SIoT.Weather.Station.Api.Interface;

namespace SIoT.Weather.Station.Api.Helper
{
    public class RequestParameterValidator : IRequestParameterValidator
    {
        public bool ParametersForGetAllDataPointsOfOneDayForOneSensorTypeAreValid(string deviceId, DateTime date, string sensorType)
        {
            return !(string.IsNullOrWhiteSpace(deviceId) ||
                   date == default ||
                   string.IsNullOrWhiteSpace(sensorType));
        }

        public bool ParametersForGetDataPointsOfOneDayAreValid(string deviceId, DateTime date)
        {
            return !(string.IsNullOrWhiteSpace(deviceId) ||
                     date == default);
        }
    }
}
