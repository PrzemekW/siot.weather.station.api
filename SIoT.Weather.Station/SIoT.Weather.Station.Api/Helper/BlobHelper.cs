﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIoT.Weather.Station.Api.Extensions;
using SIoT.Weather.Station.Api.Interface;
using SIoT.Weather.Station.Api.Model;

namespace SIoT.Weather.Station.Api.Helper
{
    public class BlobHelper : IBlobHelper
    {
        public string BuildBlobName(string deviceId, DateTime date, string sensorType)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                throw new ArgumentNullException(nameof(deviceId));
            }

            if (string.IsNullOrWhiteSpace(sensorType))
            {
                throw new ArgumentNullException(nameof(sensorType));
            }

            if (date == default)
            {
                throw new ArgumentException($"{nameof(date)} cannot be default date value({default(DateTime)}");
            }

            var month = date.Month < 10 ? $"0{date.Month}" : date.Month.ToString();
            var day = date.Day < 10 ? $"0{date.Day}" : date.Day.ToString();

            return $"{deviceId}/{sensorType}/{date.Year}-{month}-{day}.csv";

        }

        public string BuildHistoricalBLobName(string deviceId, string sensorType)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                throw new ArgumentNullException(nameof(deviceId));
            }

            if (string.IsNullOrWhiteSpace(sensorType))
            {
                throw new ArgumentNullException(nameof(sensorType));
            }

            return $"{deviceId}/{sensorType}/historical.zip";
        }

        public async Task<List<SensorDataPoint>> GetSensorDataPoints(BlobByteResult blobBytesResult, DateTime date)
        {
            if (blobBytesResult == null)
            {
                throw new ArgumentNullException(nameof(blobBytesResult));
            }

            if (blobBytesResult.Bytes.Length == 0)
            {
                return null;
            }

            if (blobBytesResult.HistoricalData)
            {

                return await GetDataPointsFromHistorical(blobBytesResult.Bytes, date);
            }
            else
            {
                return GetDataPointsFromCurrent(blobBytesResult.Bytes);
            }
        }

        private async Task<List<SensorDataPoint>> GetDataPointsFromHistorical(byte[] bytes, DateTime date)
        {
            var month = date.Month < 10 ? $"0{date.Month}" : date.Month.ToString();
            var day = date.Day < 10 ? $"0{date.Day}" : date.Day.ToString();
            var blobName = $"{date.Year}-{month}-{day}.csv";
            var content = string.Empty;
            using (var stream = new MemoryStream(bytes))
            {
                using (var zipArchive = new ZipArchive(stream, ZipArchiveMode.Read))
                {
                    var entry = zipArchive.Entries.SingleOrDefault(x => x.FullName == blobName);

                    if (entry != null)
                    {
                        using (var streamReader = new StreamReader(entry.Open()))
                        {
                            content = await streamReader.ReadToEndAsync();
                        }
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(content))
            {
                return null;
            }

            var dataPointList = ParseToDataPointList(content);

            return dataPointList;
        }

        private static List<SensorDataPoint> GetDataPointsFromCurrent(byte[] bytes)
        {
            var content = Encoding.UTF8.GetString(bytes);

            var sensorDataPointList = ParseToDataPointList(content);

            return sensorDataPointList;
        }

        private static List<SensorDataPoint> ParseToDataPointList(string content)
        {
            return content.Replace(',', '.').Split("\r\n").Where(x => !string.IsNullOrWhiteSpace(x)).Select(
                x => x.MapToSensorDataPoint()).Where(x => x != null).ToList();
        }
    }
}